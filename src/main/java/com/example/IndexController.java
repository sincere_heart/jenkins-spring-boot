package com.example;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liheng
 * @since 2021-01-26
 */
@RestController
@RequestMapping("/food-order")
public class IndexController {

    @RequestMapping("/")
    public String sele(){
        return "foodOrderService.getById(1)";
    }
}

